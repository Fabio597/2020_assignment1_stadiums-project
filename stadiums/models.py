from django import forms
from django.db import models


class Nation(models.Model):
    name = models.CharField(max_length=80, default=None, null=True)


class NationForm(forms.ModelForm):
    class Meta:
        model = Nation
        fields = (
            'name',
        )


class City(models.Model):
    name = models.CharField(max_length=80, default=None, null=True)
    nation = models.ForeignKey(Nation, on_delete=models.CASCADE, null=True)


class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = (
            'name',
            'nation'
        )


class Stadium(models.Model):
    name = models.CharField(max_length=200, default=None)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default=None)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default=None)
    capacity = models.PositiveIntegerField(null=False, default=0)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True)


class StadiumForm(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = (
            'name',
            'latitude',
            'longitude',
            'capacity',
            'city'
        )
