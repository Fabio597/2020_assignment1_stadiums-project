from django.test import TestCase
from stadiums.models import Nation, City, Stadium
from stadiums.stadiums import *

class UtilsTest(TestCase):

    def setUp(self):
        #Nations
        self.first_nation = Nation(name="Portugal")
        self.second_nation = Nation(name="Spain")

        #cities
        self.madrid = City(name="Madrid", nation=self.second_nation)
        self.porto = City(name="Porto", nation=self.first_nation)

        #Stadiums
        self.madrid_stadium = Stadium(
            name="Santiago Bernabeu",
            latitude=40.45306,
            longitude=-3.68835,
            capacity=81044,
            city=self.madrid
        )
        self.porto_stadium = Stadium(
            name="Estadio do Dragao",
            latitude=41.161758,
            longitude=-8.583933,
            capacity=50083,
            city=self.porto
        )

    # Tests for Stadiums object ---------------------------------------

    def create_stadiums_object_empty(self):
        stadiums = []
        self.empty_stadium_obj = Stadiums(stadiums)

    def create_stadiums_object_for_tests(self):
        stadiums = []
        stadiums.append(self.madrid_stadium)
        stadiums.append(self.porto_stadium)
        self.stadiums_obj = Stadiums(stadiums)

    def test_stadium_object_creation_first(self):
        self.assertEqual(self.madrid_stadium.name, "Santiago Bernabeu")
        self.assertEqual(self.madrid_stadium.latitude, 40.45306)
        self.assertEqual(self.madrid_stadium.longitude, -3.68835)
        self.assertEqual(self.madrid_stadium.capacity, 81044)
        self.assertEqual(self.madrid_stadium.city.name, "Madrid")
        self.assertEqual(self.madrid_stadium.city.nation.name, "Spain")

    def test_stadium_object_creation_second(self):
        self.assertEqual(self.porto_stadium.name, "Estadio do Dragao")
        self.assertEqual(self.porto_stadium.latitude, 41.161758)
        self.assertEqual(self.porto_stadium.longitude, -8.583933)
        self.assertEqual(self.porto_stadium.capacity, 50083)
        self.assertEqual(self.porto_stadium.city.name, "Porto")
        self.assertEqual(self.porto_stadium.city.nation.name, "Portugal")

    def test_stadium_capacity_empty(self):
        self.create_stadiums_object_empty()
        self.assertEqual(self.empty_stadium_obj.get_total_capacity(), 0)

    def test_stadium_capacity_average_empty(self):
        self.create_stadiums_object_empty()
        self.assertEqual(self.empty_stadium_obj.get_total_capacity_average(), 0)

    def test_stadium_capacity_max_empty(self):
        self.create_stadiums_object_empty()
        self.assertEqual(self.empty_stadium_obj.get_max_capacity(), 0)

    def test_stadium_variance_empty(self):
        self.create_stadiums_object_empty()
        self.assertEqual(self.empty_stadium_obj.get_variance(), 0)

    def test_stadium_standard_deviation_empty(self):
        self.create_stadiums_object_empty()
        self.assertEqual(self.empty_stadium_obj.get_standard_deviation(), 0)

    def test_stadium_capacity(self):
        self.create_stadiums_object_for_tests()
        self.assertEqual(self.stadiums_obj.get_total_capacity(), 131127)

    def test_stadium_capacity_average(self):
        self.create_stadiums_object_for_tests()
        self.assertEqual(self.stadiums_obj.get_total_capacity_average(), 65563.5)

    def test_stadium_capacity_max(self):
        self.create_stadiums_object_for_tests()
        self.assertEqual(self.stadiums_obj.get_max_capacity(), 81044)

    def test_stadium_variance(self):
        self.create_stadiums_object_for_tests()
        self.assertEqual(self.stadiums_obj.get_variance(), 239645880.25)

    def test_stadium_standard_deviation(self):
        self.create_stadiums_object_for_tests()
        self.assertEqual(self.stadiums_obj.get_standard_deviation(), 15480.5)

    # End Tests for Stadiums object ---------------------------------------
