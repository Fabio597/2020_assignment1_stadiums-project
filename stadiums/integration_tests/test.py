from django.test import TestCase
from rest_framework.test import APIClient
from stadiums.db_operations import *

class ChartsWebappDjangoIntegrationTests(TestCase):

    def setUp(self):
        self.client = APIClient()
        #Nations
        self.first_nation = Nation(name="Portugal")
        self.second_nation = Nation(name="Spain")

        #cities
        self.madrid = City(name="Madrid", nation=self.second_nation)
        self.porto = City(name="Porto", nation=self.first_nation)

        #Stadiums
        self.madrid_stadium = Stadium(
            name="Santiago Bernabeu",
            latitude=40.45306,
            longitude=-3.68835,
            capacity=81044,
            city=self.madrid
        )
        self.porto_stadium = Stadium(
            name="Estadio do Dragao",
            latitude=41.161758,
            longitude=-8.583933,
            capacity=50083,
            city=self.porto
        )

# test api of django webapp (only response status)

    def test_nation_api(self):
        response = self.client.get('/api/nations/')
        self.assertEqual(response.status_code, 200)

    def test_cities_api(self):
        response = self.client.get('/api/nations/')
        self.assertEqual(response.status_code, 200)

    def test_stadiums_api(self):
        response = self.client.get('/api/nations/')
        self.assertEqual(response.status_code, 200)

# DataBase Operations test --------------------------------------------
    def test_save_nation_first(self):
        save_nation(self.first_nation.name)

        all_nations = get_all_nations()
        self.assertEqual(len(all_nations), 1)
        self.assertEqual(all_nations[0].name, "Portugal")

    def test_save_nation_second(self):
        save_nation(self.second_nation.name)

        all_nations = get_all_nations()
        self.assertEqual(len(all_nations), 1)
        self.assertEqual(all_nations[0].name, "Spain")

    def test_save_nation_third(self):
        save_nation(self.first_nation.name)
        save_nation(self.second_nation.name)

        all_nations = get_all_nations()
        self.assertEqual(len(all_nations), 2)
        self.assertEqual(all_nations[0].name, "Portugal")
        self.assertEqual(all_nations[1].name, "Spain")

    def test_get_nation_first(self):
        self.first_nation.save()

        first = get_nation_by_id(self.first_nation.id)
        self.assertEqual(first.name, "Portugal")

    def test_get_nation_second(self):
        self.second_nation.save()

        second = get_nation_by_id(self.second_nation.id)
        self.assertEqual(second.name, "Spain")

    def test_save_city_first(self):
        self.madrid.nation.save()
        save_city(self.madrid.name, self.madrid.nation)

        all_cities = get_all_cities()
        self.assertEqual(all_cities[0].name, "Madrid")
        self.assertEqual(all_cities[0].nation.name, "Spain")

    def test_save_city_second(self):
        self.porto.nation.save()
        save_city(self.porto.name, self.porto.nation)

        all_cities = get_all_cities()
        self.assertEqual(all_cities[0].name, "Porto")
        self.assertEqual(all_cities[0].nation.name, "Portugal")

    def test_save_city_third(self):
        self.madrid.nation.save()
        save_city(self.madrid.name, self.madrid.nation)
        self.porto.nation.save()
        save_city(self.porto.name, self.porto.nation)

        all_cities = get_all_cities()
        self.assertEqual(all_cities[0].name, "Madrid")
        self.assertEqual(all_cities[0].nation.name, "Spain")
        self.assertEqual(all_cities[1].name, "Porto")
        self.assertEqual(all_cities[1].nation.name, "Portugal")
        self.assertEqual(len(all_cities), 2)

    def test_get_city_first(self):
        self.madrid.nation.save()
        self.madrid.save()

        first = get_city_by_id(self.madrid.id)
        self.assertEqual(first.name, "Madrid")

    def test_get_city_second(self):
        self.porto.nation.save()
        self.porto.save()

        first = get_city_by_id(self.porto.id)
        self.assertEqual(first.name, "Porto")

    def test_save_stadium_first(self):
        self.madrid_stadium.city.nation.save()
        self.madrid_stadium.city.save()
        add_single_stadium(
            self.madrid_stadium.name,
            self.madrid_stadium.longitude,
            self.madrid_stadium.latitude,
            self.madrid_stadium.capacity,
            self.madrid_stadium.city
        )
        all_stadiums = get_all_stadiums()
        self.assertEqual(all_stadiums[0].name, "Santiago Bernabeu")
        self.assertEqual(all_stadiums[0].capacity, 81044)
        self.assertEqual(all_stadiums[0].city.name, "Madrid")


    def test_save_stadium_second(self):
        self.porto_stadium.city.nation.save()
        self.porto_stadium.city.save()
        add_single_stadium(
            self.porto_stadium.name,
            self.porto_stadium.longitude,
            self.porto_stadium.latitude,
            self.porto_stadium.capacity,
            self.porto_stadium.city
        )
        all_stadiums = get_all_stadiums()
        self.assertEqual(all_stadiums[0].name, "Estadio do Dragao")
        self.assertEqual(all_stadiums[0].capacity, 50083)
        self.assertEqual(all_stadiums[0].city.name, "Porto")

    def test_save_stadium_third(self):
        self.madrid_stadium.city.nation.save()
        self.madrid_stadium.city.save()
        add_single_stadium(
            self.madrid_stadium.name,
            self.madrid_stadium.longitude,
            self.madrid_stadium.latitude,
            self.madrid_stadium.capacity,
            self.madrid_stadium.city
        )
        self.porto_stadium.city.nation.save()
        self.porto_stadium.city.save()
        add_single_stadium(
            self.porto_stadium.name,
            self.porto_stadium.longitude,
            self.porto_stadium.latitude,
            self.porto_stadium.capacity,
            self.porto_stadium.city
        )
        all_stadiums = get_all_stadiums()
        self.assertEqual(all_stadiums[0].name, "Santiago Bernabeu")
        self.assertEqual(all_stadiums[0].capacity, 81044)
        self.assertEqual(all_stadiums[0].city.name, "Madrid")
        self.assertEqual(all_stadiums[1].name, "Estadio do Dragao")
        self.assertEqual(all_stadiums[1].capacity, 50083)
        self.assertEqual(all_stadiums[1].city.name, "Porto")
        self.assertEqual(len(all_stadiums), 2)

    # End DataBase Operations test --------------------------------------------