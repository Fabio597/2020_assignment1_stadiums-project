from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'nations', views.NationViewSet)
router.register(r'cities', views.CityViewSet)
router.register(r'stadiums', views.StadiumViewSet)

urlpatterns = [
    path('', views.index, name='index'),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('add_data', views.add_data, name='add_data'),
    path('add_data/add_nation/', views.add_nation, name='add_nation'),
    path('add_data/add_city/', views.add_city, name='add_city'),
    path('add_data/add_stadium/', views.add_stadium, name='add_stadium')
]
