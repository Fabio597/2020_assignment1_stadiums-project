import math

class Stadiums:

    def __init__(self, stadium_list):
        self.stadiums_list = stadium_list

    def get_total_capacity(self):
        total = 0
        for stadium in self.stadiums_list:
            total = total + stadium.capacity
        return total

    def get_total_capacity_average(self):
        total = self.get_total_capacity()
        if len(self.stadiums_list) != 0:
            return total / len(self.stadiums_list)
        return 0

    def get_max_capacity(self):
        max_value = 0
        for stadium in self.stadiums_list:
            if stadium.capacity > max_value:
                max_value = stadium.capacity
        return max_value

    def get_variance(self):
        if len(self.stadiums_list) != 0:
            mean = self.get_total_capacity_average()
            sum_value = 0
            for stadium in self.stadiums_list:
                sum_value = sum_value + ((stadium.capacity - mean) * (stadium.capacity - mean))
            return sum_value / len(self.stadiums_list)
        return 0

    def get_standard_deviation(self):
        variance = self.get_variance()
        return math.sqrt(variance)
