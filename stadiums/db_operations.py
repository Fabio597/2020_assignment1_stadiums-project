from .models import Nation, City, Stadium
def save_nation(nation_name):
    nation_to_add = Nation(name=nation_name)
    nation_to_add.save()


def get_nation_by_id(nation_id):
    return Nation.objects.get(id=nation_id)


def get_all_nations():
    return Nation.objects.all()


def save_city(city_name, city_nation):
    city_to_add = City(name=city_name, nation=city_nation)
    city_to_add.save()


def get_city_by_id(city_id):
    return City.objects.get(id=city_id)


def get_all_cities():
    return City.objects.all()


def add_single_stadium(stadium_name,
                       stadium_longitude,
                       stadium_latitude,
                       stadium_capacity,
                       stadium_city):
    stadium_to_add = Stadium(
        name=stadium_name,
        latitude=stadium_latitude,
        longitude=stadium_longitude,
        capacity=stadium_capacity,
        city=stadium_city
    )
    stadium_to_add.save()


def get_all_stadiums():
    return Stadium.objects.all()
