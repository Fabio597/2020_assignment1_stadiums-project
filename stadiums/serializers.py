from rest_framework import serializers
from .models import Nation, City, Stadium

class NationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Nation
        fields = ('name',)


class CitySerializer(serializers.HyperlinkedModelSerializer):
    nation = NationSerializer(many=False, read_only=True)
    class Meta:
        model = City
        fields = ('name', 'nation')


class StadiumSerializer(serializers.HyperlinkedModelSerializer):
    city = CitySerializer(many=False, read_only=True)
    class Meta:
        model = Stadium
        fields = ('name', 'latitude', 'longitude', 'capacity', 'city')
