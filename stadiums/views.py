from django.shortcuts import render, redirect
from rest_framework import viewsets
from .db_operations import get_all_nations, get_all_cities, get_all_stadiums,\
    save_nation, save_city, get_nation_by_id,\
    get_city_by_id, add_single_stadium
from .serializers import NationSerializer, CitySerializer, StadiumSerializer
from .stadiums import Stadiums
from .models import Nation, City, Stadium

def index(request):
    stadiums = get_all_stadiums()
    stadiums_obj = Stadiums(stadiums)
    return render(request, 'stadiums/index.html', {
        'stadiums': stadiums_obj,
        'total_capacity': stadiums_obj.get_total_capacity(),
        'total_capacity_average': stadiums_obj.get_total_capacity_average()
    })


def add_data(request):
    nations = get_all_nations()
    cities = get_all_cities()
    return render(request, 'stadiums/add_data.html', {
        'nations': nations,
        'cities': cities
    })

def add_nation(request):
    nation = request.POST.get('inputNation', None)
    if nation is not None:
        save_nation(nation)
        return redirect('index')
    return redirect('index')


def add_city(request):
    city_name = request.POST.get('inputCity', None)
    nation_id = request.POST.get('inputState', None)
    if city_name is not None and nation_id is not None:
        city_nation = get_nation_by_id(nation_id)
        save_city(city_name, city_nation)
        return redirect('index')
    return redirect('index')


def add_stadium(request):
    name = request.POST.get('inputStadium', None)
    longitude = request.POST.get('inputLongitude', None)
    latitude = request.POST.get('inputLatitude', None)
    capacity = request.POST.get('inputCapacity', None)
    city_id = request.POST.get('stadiumCity', None)
    stadium_city = get_city_by_id(city_id)
    add_single_stadium(name, longitude, latitude, capacity, stadium_city)
    return redirect('index')

class NationViewSet(viewsets.ModelViewSet):
    queryset = Nation.objects.all()
    serializer_class = NationSerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer


class StadiumViewSet(viewsets.ModelViewSet):
    queryset = Stadium.objects.all()
    serializer_class = StadiumSerializer
