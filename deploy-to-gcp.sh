#!/bin/bash

ssh -o PreferredAuthentications=publickey -p $SSH_PORT $SSH_USERNAME@$IP_ADDRESS \
"docker login registry.gitlab.com -u gitlab-ci-token -p $READ_WRITE_TOKEN \
&& docker stop mysql || true \
&& docker rm -f stadiums_project || true \
&& docker pull registry.gitlab.com/fabio597/2020_assignment1_stadiums-project \
&& docker-compose up -d \
&& exit"

