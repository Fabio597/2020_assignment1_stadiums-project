from setuptools import find_packages, setup
setup(
    name='stadiums',
    version='0.1.0',
    include_package_data=True,
    packages=['stadiums_project', '.'],
    zip_safe=False,
)