# Stadiums Project

Stadiums Project is a Python webapp developed with Django framework for the first Assignment of the course Process and Software developing.
This is a simple webapp that enables you to add a list of stadiums and check some properties of this stadiums list.
We provide also some API to reach the list of nations, cities and stadiums that are saved in the Database.
API are available on:
1. Nations: _/api/nations_
2. Cities: _/api/cities_
3. Stadiums: _/api/stadiums_

The purpose of this project is the pipeline so we don't perform any check on forms used to populate Database, so if you want to add data, please fill all fields.
On the main page you can see the list of stadiums added and below the table we show the total capacity of all stadiums and the mean of the capacity.
## Installation

Use Docker to run this project, we built a Docker container for the python project and a MYSQL Container used to store information. So run the following line in the directory of the project  (this will launch the "develop" docker-compose.yml file of the web application):
```
docker-compose up
```

## Database
We implemented a simple structure for our DB. We store information only regarding Nations, Cities and Stadiums, to display them on the main screen in the UI of the webapp.
Here there is the Entity Relationship of the data stored in the DataBase.
![image info](./pictures/databaseER.png)
We use an instance of mysql:5.7 image to store information. MYSQL is linked with django webapp by docker-compose file.
So we can say that we have two components in our ptoject: Django-Webapp and MYSQL DataBase

## Gitlab-Runner
To execute the pipeline we use the Gitlab-Runner that we installed on a Virtual Machine on Google Cloud Platform (GCM).
We gave to the Runner all the permission to execute commands to perform jobs on our project.
The runner installed on our Virtual Machine allows us to avoid using Shared Runners 

## Pipeline
Here we explaine what every stage of the pipeline does. We decided to trigger all stages of the pipeline when __Master__ branch is pushed because it means that a new incremental step (new features, bug fix, ecc...) in our software is ready to be deployed.
So if someone pushes the changes to another branch (like develop or a feature branch) it won't trigger all jobs of the pipeline but only the first 4 jobs (build, verify, unit tests and integration tests) because we want to avoid the package, release and deploy of a feature that could not be ready for the production environment so for our customers.
We used a global cache for our pipeline because we don't have a lot of dependencies, so we can install them only in the first stage (the build one) and we can use those dependencies also for other jobs except release and deploy. 

1. BUILD: In this stage we want to verify that all packages that are used in the project are correctly installed, so nothing unexpected happen during the installation. What we install? Django, mysqlclient(to connect to mysql database), restframework, ecc..
2. VERIFY: In this stage we analyze the static code with _pylint_ for python code and with __curlylint__ for HTML code . The code analyzed is done in parallel for Python and HTML code with two jobs called: __verify_python__ and __verify_html__. We added two files called .pylintrc and pyproject.py respectively for pylint and curlylint where we enable/disable rules for these linters. These jobs could fail (allow_failure = true) because we make only some checks on the style of our python and HTML code (linting), so it won't be a problem for the performances of our webapp. Only for python code, if there are any issues, we save a report in the job inside a file called "pylint_errors.txt", so if needed we can download that file to see reported errors. We enable only 2 or 3 rules just to show the linting process. 
3. UNIT TESTS: In this stage we perform unit tests, so we test small unit of the software like functions. We perform tests on Businness object of the Web Application like Stadiums object and his methods that calculate some statistical values regarding stadiums list.
4. INTEGRATION TESTS: In this stage we perform integration tests, so we test the integration of other modules with our django app. In our case we test methods used to interact with MYSQL Database and APIs. In this case we wrote some tests to perform the integration with MYSQL database and then we verify the status of our database after these operation to check that information has been stored correctly. We also perform test on APIs because values returned are taken from our Database (so we consider them as integration tests).
5. PACKAGE: In this stage we prepare the app packaged to be released in a production environment. We use _whell_ to package the webapp and create a _*.whl_ file to be deployed next. This file is saved in this job.
6. RELEASE: In this stage we create a docker image useful to launch our project with a MYSQL container. We create only the image for Django Webapp (stadiums_project) and we push it on our Gitlab Container Registry. This image is created by a production Dockerfile because we use the package created by the previous job and we install ad unzip it during the creation of the docker image.
7. DEPLOY: In this stage (the last one) we deploy our project on production environment using an ssh connection. We connect via ssh to the Virtual Machine on Google Cloud Platform and then we send via scp the docker-compose.yml file used for the production environment and in the end we login, pull and launch the new container instance from th latest image and we restart/create the MYSQL container where we store data. 

## Work Done by
Fabio D'Adda 817279 f.dadda4@campus.unimib.it  
Mario Enrico Centrone 816325 m.centrone2@campus.unimib.it  
The Project is deployed on a Virtual Machine on Google Cloud Platform, the ip address where you can find it is: http://35.188.38.160:8080/
Link to the repository: https://gitlab.com/Fabio597/2020_assignment1_stadiums-project