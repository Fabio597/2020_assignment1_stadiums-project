FROM python:3.6
CMD ["/sbin/my_init"]
ENV PYTHONUNBUFFERED 1

RUN mkdir /stadiums_project
WORKDIR /stadiums_project
ADD . /stadiums_project/
RUN pip3 install --no-cache-dir -r requirements.txt